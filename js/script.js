var data = {};
$(function () {
    var categoriesUrl = "https://api.spotify.com/v1/browse/categories?country=US"; /* Categories */
    var token = "BQBKMiuE-sW1L0JPds5h-7mNYTdf7_3bHZ66dWfMCAwY2EtKDAG9BKswqmvHthIG6aMtQrLkXJAFSUayUZ4zF3L8Yy_DLlkxZ5pKnNH334uBSIjl-sSJNpz7DjCIXUbeSAWSFXfX5hniu3HQfMHPLKZNrQ4kBj8";

    if ($(".kategorije").length){
        $.ajax({
            type: "GET", // GET, POST, PUT
            url: categoriesUrl, // the url to call
            contentType: "JSON",
            beforeSend: function (xhr) { // Set token here
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        }).done(function (categoryData) {
            data.categories = categoryData;       
            var template = $("#categoriesTemplate").html();
            $(".kategorije").html(_.template(template));
        });
    }

    /* LOGO TO HOMEPAGE */ 
    $(document).on("click", "#navbar-logo", function(){
            window.location.href = "index.html";
        });

    /* PLAYLIST LINKS START */
    $(document).on("click", ".category-name", function (event) {
        var playlistButton = $(event.target).data("id"); // Category name on click
        var playlistDiv = $("#playlist-links");
        playlistDiv.empty()
        var songDiv = $("#track-links");
        songDiv.empty();
        var playlistUrl = "https://api.spotify.com/v1/browse/categories/" + playlistButton + "/playlists?country=US"; // Playlists 
        $.ajax({
            type: "GET",
            url: playlistUrl,
            contentType: "JSON",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        }).done(function (playlistData) {
            data.playlists = playlistData;
            var template = $("#playlistsTemplate").html();
            $(".playliste").html(_.template(template));
            // console.log(data.playlists);
        });
        /* SONG LINKS START */
        $(document).on("click", ".playlist-name", function (event) {
            var songButton = $(event.target).data("id"); // Playlists id on click
            var playlistDiv = $("#playlist-links");
            playlistDiv.empty()
            var songsUrl = "https://api.spotify.com/v1/playlists/" + songButton + "/tracks?market=US&limit=20"; // Tracks 
            $.ajax({
                type: "GET",
                url: songsUrl,
                contentType: "JSON",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                }
            }).done(function (songsData) { // Playlist data, items..
                data.songs =  songsData;
                // console.log(songsData.items.track.id);
                var template = $("#songsTemplate").html();
                $(".pjesme").html(_.template(template));

                $(".saveSong").on("click", function() {
                    swal("Song added to favorites!", " ", "success");
                });
            });
        });
        /* SONG LINKS END */
    });
    /* PLAYLIST LINKS END */

    /* SAVING SONG URLs TO LOCAL STORAGE */
    $(document).on("click", ".saveSong", function (event) {
        var trackUrl = $(event.currentTarget).data("track"); // Track URL
        var favorites = localStorage.getItem("trackUrl") === null ? [] : JSON.parse(localStorage.getItem("trackUrl"));
        favorites.push(trackUrl);
        localStorage.setItem("trackUrl", JSON.stringify(favorites));
    });

    /* LOADING FAVORITES ON FAVORITES PAGE*/
    if ($(".favorites-page").length) {
        var favoritesList = JSON.parse(localStorage.getItem("trackUrl")); // Favorites ID
        $.each(favoritesList, function (index, item) {
            var favoritesUrl = "https://api.spotify.com/v1/tracks/" + item
            $.ajax({
                type: "GET",
                url: favoritesUrl,
                contentType: "JSON",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                }
            }).done(function (favoritesData) { // Favorites data, items..
                if (data.favorites === undefined) {
                    data.favorites = [];
                }
                data.favorites.push(favoritesData);
                
                var template = $("#favoritesTemplate").html();
                $("#favoriti").html(_.template(template));

                $(".removeSong").on("click", function() {
                    swal("Song removed from favorites!", " ", "success");
                });
            });
            // DONE ENDING
        });
        // EACH ENDING
        $(document).on("click", ".removeSong", function(event) {
            var favoritesList = JSON.parse(localStorage.getItem("trackUrl")); // List of favorites in local storage
            var clickId = $(event.currentTarget).data("id"); // Clicked song id
            $.each(favoritesList, function(index, item) {
                if (clickId == item) {
                    favoritesList.splice(index, 1);
                    localStorage.setItem("trackUrl", JSON.stringify(favoritesList));
                    $(event.currentTarget).closest(".card").remove();
                }
            });
        });
    };
    // IF STATEMENT ENDING

    // CONTACT FORM
    if($("#contact-page").length) {
        $("#submit").button();
        // SAVING USER INFO TO LOCAL STORAGE
        $(document).on("click", "#submit", function(event) {
            event.preventDefault();
            if(!validate()) {
                return;
            }
            var userInfo = {
                firstName: $("#firstName").val(),
                lastName: $("#lastName").val(),
                email: $("#email").val()
            }
            var userData = localStorage.getItem("userData") === null ? [] : JSON.parse(localStorage.getItem("userData"))
            userData.push(userInfo);
            localStorage.setItem("userData", JSON.stringify(userData));
            
            $("#form")[0].reset();

            swal("User added!", " ", "success");
        });

        function validate() {
            var valid = 1;
            var firstName = $("#firstName").val();
            var lastName = $("#lastName").val();
            var email = $("#email").val();

            /* FIRST NAME VALIDATION */
            if(!isValidFirstName(firstName)) {
                valid = 0;
                $("input#firstName").css({"box-shadow": "0 0 20px red", "border": "2px solid red"});
                $("#firstName-feedback").text("Please enter at least two characters").css("color", "red");
            } else {
                $("input#firstName").css({"box-shadow": "0 0 20px green", "border": "2px solid green"});
                $("#firstName-feedback").text("");
            }

            /* LAST NAME VALIDATION */
            if(!isValidLastName(lastName)) {
                valid = 0;
                $("input#lastName").css({"box-shadow": "0 0 20px red", "border": "2px solid red"});
                $("#lastName-feedback").text("Please enter at least two characters").css("color", "red");
            } else {
                $("input#lastName").css({"box-shadow": "0 0 20px green", "border": "2px solid green"});
                $("#lastName-feedback").text("");
            }

            /* EMAIL VALIDATION */
            if(!isValidEmail(email)) {
                valid = 0;
                $("input#email").css({"box-shadow": "0 0 20px red", "border": "2px solid red"});
                $("#email-feedback").text("Please enter a valid email address").css("color", "red");
            } else {
                $("input#email").css({"box-shadow": "0 0 20px green", "border": "2px solid green"});
                $("#email-feedback").text("");
            }

            return valid;

            function isValidFirstName(firstName) {
                return firstName.length >= 2;
            }
            function isValidLastName(lastName) {
                return lastName.length >= 2;
            }
            function isValidEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }
    
        }

        /* DIALOG PROPERTIES */
        $("#dialogOpener").button();
        $("#dialog").dialog({
            autoOpen : false, 
            modal : true, 
            show : "blind", 
            hide : "blind",
            minWidth: $(".ui-dialog-content").width(),
            minHeight: $(".ui-dialog-content").height() + 100
        });
        // OPENING DIALOG
        $(document).on("click", "#dialogOpener", function(event){
            $("#dialog").dialog("open");
            var userData = JSON.parse(localStorage.getItem("userData"))
            data.user = [];
            $.each(userData, function(index, item) {                
                data.user.push(item);
                // console.log(data.user);
                var template = $("#contactsTemplate").html();
                $(".userInfo").html(_.template(template));
            });
            
            $("#sortableList").sortable({
                cursor: "grabbing",
                axis: "y"
            });

        });
        $("#removeUser").button();
        // REMOVING USER FROM LOCAL STORAGE
        $(document).on("click", "#removeUser", function(event) {
            var userData = JSON.parse(localStorage.getItem("userData")); // List of favorites in local storage
            var clicked = $(event.currentTarget).data("index");
            // console.log(clicked);
            $.each(userData, function(index, item) {
                if(clicked == index) {
                    userData.splice(index, 1);
                    localStorage.setItem("userData", JSON.stringify(userData))
                    $(event.currentTarget).closest("ul").remove();

                    swal("User removed!", " ", "success");
                }
            });

        });
    }

    // function myFunction() {
    //     var links = $("#myLinks");
    //     if(links.css("display", "block") === true) {
    //         return links.css("display", "none")
    //     } else {
    //         return links.css("display", "block")
    //     }
    // }
    
    $(document).on("click", "#hamburger-btn", function() {
        $("#myLinks").fadeToggle();
    });
});


// function myFunction() {
//     var links = document.getElementById("myLinks");
//     if (links.style.display === "block") {
//       links.style.display = "none";
//     } else {
//       links.style.display = "block";
//     }
//   }